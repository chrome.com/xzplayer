# xzplayer 说明

该项目主要是基于  
+ ffmpeg 4.2.1
+ SDL2 2.0.10
对现有 IJK 的改编而成新的一个播放器

## ffmpeg 代码地址
```
http://ffmpeg.org/
```

## ijk 代码地址
```
https://github.com/Bilibili/ijkplayer
```
