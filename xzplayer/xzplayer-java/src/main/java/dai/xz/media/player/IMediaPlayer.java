package dai.xz.media.player;

import android.content.Context;
import android.net.Uri;
import android.view.Surface;
import android.view.SurfaceHolder;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.Map;

import dai.xz.media.player.misc.IMediaDataSource;
import dai.xz.media.player.misc.ITrackInfo;

public interface IMediaPlayer {

    void setMediaPlayerCallBack(IMediaCallBack cb);

    void setDataSource(Context context, Uri uri)
            throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    void setDataSource(Context context, Uri uri, Map<String, String> headers)
            throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    void setDataSource(FileDescriptor fd)
            throws IOException, IllegalArgumentException, IllegalStateException;

    void setDataSource(String path)
            throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    void setDataSource(IMediaDataSource mediaDataSource);

    void prepareAsync() throws IllegalStateException;

    void start() throws IllegalStateException;

    void stop() throws IllegalStateException;

    void pause() throws IllegalStateException;

    void seekTo(long msec) throws IllegalStateException;

    void reset();

    void release();

    int getVideoWidth();

    int getVideoHeight();

    long getDuration();

    long getCurrentPosition();

    void setScreenOnWhilePlaying(boolean screenOn);

    void setSurface(Surface surface);

    void setDisplay(SurfaceHolder sh);

    int getVideoSarNum();

    int getVideoSarDen();

    boolean isPlaying();

    void setLooping(boolean looping);

    boolean isLooping();

    void setVolume(float left, float right);

    int getAudioSessionId();

    ITrackInfo[] getTrackInfo();

    MediaInfo getMediaInfo();
}
