package dai.xz.media.player.misc;

import android.media.MediaFormat;

import androidx.annotation.NonNull;

public final class AndroidMediaFormat implements IMediaFormat {
    private final MediaFormat mMediaFormat;

    public AndroidMediaFormat(MediaFormat mediaFormat) {
        mMediaFormat = mediaFormat;
    }

    @Override
    public String getString(String name) {
        if (null == mMediaFormat) {
            return null;
        }
        return mMediaFormat.getString(name);
    }

    @Override
    public int getInteger(String name) {
        if (null == mMediaFormat) {
            return 0;
        }
        return mMediaFormat.getInteger(name);
    }

    @NonNull
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append(getClass().getName());
        sb.append('{');
        if (mMediaFormat != null) {
            sb.append(mMediaFormat.toString());
        } else {
            sb.append("null");
        }
        sb.append('}');
        return sb.toString();
    }
}
