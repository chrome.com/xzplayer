package dai.xz.media.player.misc;

import android.text.TextUtils;

import androidx.annotation.NonNull;

public class XZTrackInfo implements ITrackInfo {
    private int mTrackType = MEDIA_TRACK_TYPE_UNKNOWN;
    private XZMediaMeta.XZStreamMeta mStreamMeta;

    public XZTrackInfo(XZMediaMeta.XZStreamMeta meta) {
        mStreamMeta = meta;
    }

    public void setMediaMeta(XZMediaMeta.XZStreamMeta meta) {
        mStreamMeta = meta;
    }

    @Override
    public IMediaFormat getFormat() {
        return new XZMediaFormat(mStreamMeta);
    }

    @Override
    public String getLanguage() {
        if (null == mStreamMeta || TextUtils.isEmpty(mStreamMeta.mLanguage))
            return "und";
        return mStreamMeta.mLanguage;
    }

    @Override
    public int getTrackType() {
        return mTrackType;
    }

    public void setTrackType(int trackType) {
        mTrackType = trackType;
    }

    @Override
    public String getInfoInline() {
        StringBuilder out = new StringBuilder(128);
        switch (mTrackType) {
            case MEDIA_TRACK_TYPE_VIDEO:
                out.append("VIDEO");
                out.append(", ");
                out.append(mStreamMeta.getCodecShortNameInline());
                out.append(", ");
                out.append(mStreamMeta.getBitrateInline());
                out.append(", ");
                out.append(mStreamMeta.getResolutionInline());
                break;
            case MEDIA_TRACK_TYPE_AUDIO:
                out.append("AUDIO");
                out.append(", ");
                out.append(mStreamMeta.getCodecShortNameInline());
                out.append(", ");
                out.append(mStreamMeta.getBitrateInline());
                out.append(", ");
                out.append(mStreamMeta.getSampleRateInline());
                break;
            case MEDIA_TRACK_TYPE_TIMEDTEXT:
                out.append("TIMEDTEXT");
                out.append(", ");
                out.append(mStreamMeta.mLanguage);
                break;
            case MEDIA_TRACK_TYPE_SUBTITLE:
                out.append("SUBTITLE");
                break;
            default:
                out.append("UNKNOWN");
                break;
        }
        return out.toString();
    }

    @NonNull
    @Override
    public String toString() {
        return getClass().getSimpleName() + '{' + getInfoInline() + "}";
    }
}
