package dai.xz.media.player.misc;

import android.media.MediaFormat;
import android.media.MediaPlayer;

import androidx.annotation.NonNull;

public final class AndroidTrackInfo implements ITrackInfo {
    private final MediaPlayer.TrackInfo mTrackInfo;

    private AndroidTrackInfo(MediaPlayer.TrackInfo trackInfo) {
        mTrackInfo = trackInfo;
    }

    @Override
    public IMediaFormat getFormat() {
        if (null == mTrackInfo)
            return null;

        MediaFormat mediaFormat = mTrackInfo.getFormat();
        if (null == mediaFormat)
            return null;

        return new AndroidMediaFormat(mediaFormat);
    }

    @Override
    public String getLanguage() {
        if (null == mTrackInfo)
            return "und";
        return mTrackInfo.getLanguage();
    }

    @Override
    public int getTrackType() {
        if (null == mTrackInfo)
            return MEDIA_TRACK_TYPE_UNKNOWN;
        return mTrackInfo.getTrackType();
    }

    @Override
    public String getInfoInline() {
        if (null == mTrackInfo)
            return "null";
        return mTrackInfo.toString();
    }

    @NonNull
    @Override
    public String toString() {
        StringBuilder out = new StringBuilder(128);
        out.append(getClass().getSimpleName());
        out.append('{');
        if (mTrackInfo != null) {
            out.append(mTrackInfo.toString());
        } else {
            out.append("null");
        }
        out.append('}');
        return out.toString();
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // static function
    //
    private static AndroidTrackInfo[] fromTrackInfo(MediaPlayer.TrackInfo[] trackInfos) {
        if (trackInfos == null)
            return null;

        AndroidTrackInfo[] androidTrackInfo = new AndroidTrackInfo[trackInfos.length];
        for (int i = 0; i < trackInfos.length; ++i) {
            androidTrackInfo[i] = new AndroidTrackInfo(trackInfos[i]);
        }

        return androidTrackInfo;
    }

    public static AndroidTrackInfo[] fromMediaPlayer(MediaPlayer mediaPlayer) {
        return fromTrackInfo(mediaPlayer.getTrackInfo());
    }
}
