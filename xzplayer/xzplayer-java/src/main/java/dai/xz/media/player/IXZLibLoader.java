package dai.xz.media.player;

public interface IXZLibLoader {

    void loadLibrary(String libName) throws UnsatisfiedLinkError, SecurityException;

}
