package dai.xz.media.player;

import dai.xz.media.player.misc.XZTimedText;

public abstract class AbstractMediaPlayer implements IMediaPlayer {

    private IMediaCallBack mMediaCallBack;
    private final Object MediaCallBackLocker = new Object();

    public final void setMediaPlayerCallBack(IMediaCallBack callBack) {
        synchronized (MediaCallBackLocker) {
            mMediaCallBack = callBack;
        }
    }

    public final void resetListeners() {
        setMediaPlayerCallBack(null);
    }

    protected final void notifyOnPrepared() {
        if (null != mMediaCallBack) {
            mMediaCallBack.onPrepared();
        }
    }

    protected final void notifyOnCompletion() {
        if (null != mMediaCallBack) {
            mMediaCallBack.onCompletion();
        }
    }

    protected final void notifyOnBufferingUpdate(int percent) {
        if (null != mMediaCallBack) {
            mMediaCallBack.onBufferingUpdate(percent);
        }
    }

    protected final void notifyOnSeekComplete() {
        if (null != mMediaCallBack) {
            mMediaCallBack.onSeekComplete();
        }
    }

    protected final void notifyOnVideoSizeChanged(int width, int height, int sarNum, int sarDen) {
        if (null != mMediaCallBack) {
            mMediaCallBack.onVideoSizeChanged(width, height, sarNum, sarDen);
        }
    }

    protected final boolean notifyOnError(int what, int extra) {
        return null != mMediaCallBack && mMediaCallBack.onError(what, extra);
    }

    protected final boolean notifyOnInfo(int what, int extra) {
        return null != mMediaCallBack && mMediaCallBack.onInfo(what, extra);
    }

    protected final void notifyOnTimedText(XZTimedText text) {
        if (null != mMediaCallBack) {
            mMediaCallBack.onTimedText(text);
        }
    }

}
