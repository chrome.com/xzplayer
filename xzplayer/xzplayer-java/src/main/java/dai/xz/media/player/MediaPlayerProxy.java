package dai.xz.media.player;

import android.content.Context;
import android.net.Uri;
import android.view.Surface;
import android.view.SurfaceHolder;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.Map;

import dai.xz.media.player.misc.IMediaDataSource;
import dai.xz.media.player.misc.ITrackInfo;
import dai.xz.media.player.misc.XZTimedText;

public class MediaPlayerProxy implements IMediaPlayer {
    protected final IMediaPlayer mBackEndMediaPlayer;

    public MediaPlayerProxy(IMediaPlayer backEndMediaPlayer) {
        mBackEndMediaPlayer = backEndMediaPlayer;
    }

    public IMediaPlayer getInternalMediaPlayer() {
        return mBackEndMediaPlayer;
    }

    @Override
    public void setMediaPlayerCallBack(IMediaCallBack cb) {
        if (null == cb) {
            mBackEndMediaPlayer.setMediaPlayerCallBack(null);
            return;
        }

        final IMediaCallBack callBack = cb;
        mBackEndMediaPlayer.setMediaPlayerCallBack(new IMediaCallBack() {
            @Override
            public void onPrepared() {
                callBack.onPrepared();
            }

            @Override
            public void onCompletion() {
                callBack.onCompletion();
            }

            @Override
            public void onBufferingUpdate(int percent) {
                callBack.onBufferingUpdate(percent);
            }

            @Override
            public void onSeekComplete() {
                callBack.onSeekComplete();
            }

            @Override
            public void onVideoSizeChanged(int width, int height, int sar_num, int sar_den) {
                callBack.onVideoSizeChanged(width, height, sar_num, sar_den);
            }

            @Override
            public boolean onError(int what, int extra) {
                return callBack.onError(what, extra);
            }

            @Override
            public boolean onInfo(int what, int extra) {
                return callBack.onInfo(what, extra);
            }

            @Override
            public void onTimedText(XZTimedText text) {
                callBack.onTimedText(text);
            }
        });
    }

    @Override
    public void setDataSource(Context context, Uri uri) throws
            IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        mBackEndMediaPlayer.setDataSource(context, uri);
    }

    @Override
    public void setDataSource(Context context, Uri uri, Map<String, String> headers) throws
            IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        mBackEndMediaPlayer.setDataSource(context, uri, headers);
    }

    @Override
    public void setDataSource(FileDescriptor fd) throws
            IOException, IllegalArgumentException, IllegalStateException {
        mBackEndMediaPlayer.setDataSource(fd);
    }

    @Override
    public void setDataSource(String path) throws
            IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        mBackEndMediaPlayer.setDataSource(path);
    }

    @Override
    public void setDataSource(IMediaDataSource mediaDataSource) {
        mBackEndMediaPlayer.setDataSource(mediaDataSource);
    }

    @Override
    public void prepareAsync() throws IllegalStateException {
        mBackEndMediaPlayer.prepareAsync();
    }

    @Override
    public void start() throws IllegalStateException {
        mBackEndMediaPlayer.start();
    }

    @Override
    public void stop() throws IllegalStateException {
        mBackEndMediaPlayer.stop();
    }

    @Override
    public void pause() throws IllegalStateException {
        mBackEndMediaPlayer.pause();
    }

    @Override
    public void seekTo(long msec) throws IllegalStateException {
        mBackEndMediaPlayer.seekTo(msec);
    }

    @Override
    public void reset() {
        mBackEndMediaPlayer.reset();
    }

    @Override
    public void release() {
        mBackEndMediaPlayer.release();
    }

    @Override
    public int getVideoWidth() {
        return mBackEndMediaPlayer.getVideoWidth();
    }

    @Override
    public int getVideoHeight() {
        return mBackEndMediaPlayer.getVideoHeight();
    }

    @Override
    public long getDuration() {
        return mBackEndMediaPlayer.getDuration();
    }

    @Override
    public long getCurrentPosition() {
        return mBackEndMediaPlayer.getCurrentPosition();
    }

    @Override
    public void setScreenOnWhilePlaying(boolean screenOn) {
        mBackEndMediaPlayer.setScreenOnWhilePlaying(screenOn);
    }

    @Override
    public void setSurface(Surface surface) {
        mBackEndMediaPlayer.setSurface(surface);
    }

    @Override
    public void setDisplay(SurfaceHolder sh) {
        mBackEndMediaPlayer.setDisplay(sh);
    }

    @Override
    public int getVideoSarNum() {
        return mBackEndMediaPlayer.getVideoSarNum();
    }

    @Override
    public int getVideoSarDen() {
        return mBackEndMediaPlayer.getVideoSarDen();
    }

    @Override
    public boolean isPlaying() {
        return mBackEndMediaPlayer.isPlaying();
    }

    @Override
    public void setLooping(boolean looping) {
        mBackEndMediaPlayer.setLooping(looping);
    }

    @Override
    public boolean isLooping() {
        return mBackEndMediaPlayer.isLooping();
    }

    @Override
    public void setVolume(float left, float right) {
        mBackEndMediaPlayer.setVolume(left, right);
    }

    @Override
    public int getAudioSessionId() {
        return mBackEndMediaPlayer.getAudioSessionId();
    }

    @Override
    public ITrackInfo[] getTrackInfo() {
        return mBackEndMediaPlayer.getTrackInfo();
    }

    @Override
    public MediaInfo getMediaInfo() {
        return mBackEndMediaPlayer.getMediaInfo();
    }
}
