package dai.xz.media.player.misc;

import android.graphics.Rect;

public final class XZTimedText {
    private Rect mTextBounds = null;
    private String mTextChar = null;

    public XZTimedText(Rect bounds, String text) {
        mTextBounds = bounds;
        mTextChar = text;
    }

    public Rect getBounds() {
        return mTextBounds;
    }

    public String getText() {
        return mTextChar;
    }

}
