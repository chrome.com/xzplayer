package dai.xz.media.player;

import dai.xz.media.player.misc.XZMediaMeta;

public final class MediaInfo {
    public final String mediaPlayerName;

    public final String videoDecoder;
    public final String videoDecoderImpl;

    public final String audioDecoder;
    public final String audioDecoderImpl;

    public final XZMediaMeta meta;

    public MediaInfo(String playerName,
                     String videoDecoder, String videoDecoderImpl,
                     String audioDecoder, String audioDecoderImpl,
                     XZMediaMeta mediaMeta) {
        this.mediaPlayerName = playerName;

        this.videoDecoder = videoDecoder;
        this.videoDecoderImpl = videoDecoderImpl;

        this.audioDecoder = audioDecoder;
        this.audioDecoderImpl = audioDecoderImpl;

        this.meta = mediaMeta;
    }

    public MediaInfo(String videoDecoder, String videoDecoderImpl,
                     String audioDecoder, String audioDecoderImpl) {
        this(null, videoDecoder, videoDecoderImpl, audioDecoder, audioDecoderImpl, null);
    }

}
