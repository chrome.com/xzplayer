package dai.xz.media.player;

import dai.xz.media.player.misc.XZTimedText;

public interface IMediaCallBack {

    void onPrepared();

    void onCompletion();

    void onBufferingUpdate(int percent);

    void onSeekComplete();

    void onVideoSizeChanged(int width, int height, int sar_num, int sar_den);

    boolean onError(int what, int extra);

    boolean onInfo(int what, int extra);

    void onTimedText(XZTimedText text);
}
